 #----------------------------------------
 #     Prepare flags from make generator
 #----------------------------------------

 include conanbuildinfo.mak

 CFLAGS              += $(CONAN_CFLAGS)
 CXXFLAGS            += $(CONAN_CXXFLAGS)
 CPPFLAGS            += $(addprefix -I, $(CONAN_INCLUDE_DIRS))
 CPPFLAGS            += $(addprefix -D, $(CONAN_DEFINES))
 LDFLAGS             += $(addprefix -L, $(CONAN_LIB_DIRS))
 LDLIBS              += $(addprefix -l, $(CONAN_LIBS))
 EXELINKFLAGS        += $(CONAN_EXELINKFLAGS)

EXE_FILENAME = app

app: md5.o
	g++ md5.o $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -o $(EXE_FILENAME)

md5.o: md5.cpp
	g++ -c $(CPPFLAGS) $(CXXFLAGS) md5.cpp